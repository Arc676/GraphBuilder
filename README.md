# GraphBuilder
Mac program for building graphs with the [Pathfinder](https://github.com/Arc676/Pathfinder) library

GraphBuilder is an Objective-C++/Cocoa graphical frontend to the Pathfinder C++ library. It can be used to create graphs without manually creating the text file.
